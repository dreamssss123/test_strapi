module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', '52f39861f256351342c0735b97ecbbcf'),
    },
  },

  // Environment Var
  base_url: 'http://localhost:1337',
  default_lang: 'th',
});
